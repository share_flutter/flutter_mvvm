import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_app_demo/core/viewmodel/home_view_model.dart';
import 'package:flutter_app_demo/ui/widget/view_model_state_handler_widget.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:stacked/stacked.dart';

class HomeView extends ViewModelBuilderWidget<HomeViewModel> {
  @override
  void onViewModelReady(HomeViewModel viewModel) {
    viewModel.getInstance();
    super.onViewModelReady(viewModel);
  }

  @override
  HomeViewModel viewModelBuilder(BuildContext context) {
    return HomeViewModel();
  }

  @override
  Widget builder(BuildContext context, HomeViewModel viewModel, Widget child) {
    return ViewModelStateHandlerWidget<HomeViewModel>(
        child: Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: SafeArea(
        child: ResponsiveBuilder(
          builder: (context, device) {
            return ListView.builder(
                scrollDirection:
                    device.isMobile ? Axis.vertical : Axis.horizontal,
                itemCount: viewModel?.listData?.length ?? 0,
                itemBuilder: (context, index) {
                  var data = viewModel?.listData[index];
                  return Card(child: Text("${data?.body ?? ''}"));
                });
          },
        ),
      ),
    ));
  }
}
