import 'package:flutter_app_demo/generated/json/base/json_convert_content.dart';

class PostResponseEntity with JsonConvert<PostResponseEntity> {
  double userId;
  double id;
  String title;
  String body;
}
