import 'package:flutter_app_demo/core/model/post_response_entity.dart';
import 'package:flutter_app_demo/core/service/api_repository.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';

import '../service/dio_provider_service.dart';

class HomeViewModel extends BaseViewModel {
  RefreshController refreshController = RefreshController();
  PostResponseEntity postResponseEntity;
  List<PostResponseEntity> listData = [];
  getInstance() {
    clearErrors();
    setBusy(false);
    notifyListeners();
    getPostData();
    refreshController.refreshCompleted();
    setInitialised(true);
    notifyListeners();
  }

  getPostData() async {
    try {
      setBusy(true);
      notifyListeners();

      var response =
          await ApiRepository(DioProviderService().getDioInstance).getPosts();
      for (var data in response.data) {
        postResponseEntity = PostResponseEntity().fromJson(data);
        listData.add(postResponseEntity);
      }
    } catch (e) {
      print("error=> $e");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }
}
