import 'package:dio/dio.dart';
import 'package:flutter_app_demo/core/constant/config.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';

part 'api_repository.g.dart';

@RestApi(baseUrl: base_url)
abstract class ApiRepository {
  factory ApiRepository(Dio dio, {String baseUrl}) = _ApiRepository;

  @GET("posts")
  Future<HttpResponse> getPosts();
}
