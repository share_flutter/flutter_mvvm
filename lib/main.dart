import 'package:flutter/material.dart';
import 'package:flutter_app_demo/ui/view/home_view.dart';

import 'core/service/dio_provider_service.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    DioProviderService().getInstance();
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeView(),
    );
  }
}
